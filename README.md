# Snake

My version of the video game _Snake_, developed in Verilog HDL to run on the Xilinx Spartan-3AN FPGA, as a final project for the Laboratory of Advanced Electronics course, held in 2021 by Prof. Leonardo Ricci at the University of Trento.

## Author

MATTEO CESCATO  
Master's student at the Department of Physics of the University of Trento

## Hardware Requirements

1. Xilinx Spartan-3AN FPGA, equipped with:  
◦ 4 directional push-buttons  
◦ 4 mechanical switches  
◦ rotary knob  
◦ 8 LEDs  
◦ LCD display

2. External display with VGA port

3. PS/2 keyboard

4. Amplified speaker with 3.5 mm stereo audio jack

## Software Requirements

Xilinx, _ISE WebPACK_, version 14.7, 23rd October 2013

## Further Information

See the [report](report.pdf).

## Copyright

**Copyright © 2021, Matteo Cescato. All rights reserved.**

Please note that the logo of the University of Trento in the report belongs to the University of Trento.

