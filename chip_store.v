module  Module_Frequency_Divider  #(parameter N = 8)  (input                clk_in,
                                                       input       [N-1:0]  half_period,
                                                       input                stop,
                                                       input                reset,

                                                       output reg           clk_out);


reg  [N-1:0]  counter;

reg           reset_old;


always @(posedge clk_in) begin
        if (!reset_old & reset) begin
                counter <= 0;
                clk_out <= 1'b0;
        end else if (!stop) begin
                if (counter >= (half_period - 1'b1)) begin
                        counter <= 0;
                        clk_out <= !clk_out;
                end else begin
                        counter <= counter + 1'b1;
                end
        end

        reset_old <= reset;
end

endmodule



module  Module_Synchro_Counter  #(parameter N = 8)  (input                master_clk,
                                                     input                clk_in,
                                                     input       [N-1:0]  limit,
                                                     input                reset,

                                                     output reg  [N-1:0]  count,
                                                     output reg           carry);


reg  clk_in_old;


always @(posedge master_clk) begin
        if (reset) begin
                count <= 0;
                carry <= 1'b0;
        end else if (!clk_in_old & clk_in) begin
                if (count >= (limit - 1'b1)) begin
                        count <= 0;
                        carry <= 1'b1;
                end else if (!count) begin
                        count <= 1;
                        carry <= 1'b0;
                end else begin
                        count <= count + 1'b1;
                end
        end

        clk_in_old <= clk_in;
end

endmodule



module  Module_3_Digit_Synchro_Counter  (input          master_clk,
                                         input          clk_in,
                                         input          reset,

                                         output  [3:0]  units,
                                         output  [3:0]  tens,
                                         output  [3:0]  hundreds);


wire  w_carry_units;
wire  w_carry_tens;


Module_Synchro_Counter  #(4)  units_counter     (.master_clk(master_clk),
                                                 .clk_in(clk_in),
                                                 .limit(4'd10),
                                                 .reset(reset),

                                                 .count(units),
                                                 .carry(w_carry_units));

Module_Synchro_Counter  #(4)  tens_counter      (.master_clk(master_clk),
                                                 .clk_in(w_carry_units),
                                                 .limit(4'd10),
                                                 .reset(reset),

                                                 .count(tens),
                                                 .carry(w_carry_tens));

Module_Synchro_Counter  #(4)  hundreds_counter  (.master_clk(master_clk),
                                                 .clk_in(w_carry_tens),
                                                 .limit(4'd10),
                                                 .reset(reset),

                                                 .count(hundreds));

endmodule



module  Module_Monostable_V2  #(parameter N = 8)  (input                clk_in,
                                                   input                monostable_input,
                                                   input       [N-1:0]  duration,

                                                   output reg           monostable_output);


reg  [N-1:0]  counter;

reg           monostable_input_old;


always @(posedge clk_in) begin
        if (!counter) begin
                monostable_output <= monostable_input;

                if (monostable_input_old != monostable_input) begin
                        counter <= duration - 1'b1;
                end
        end else begin
                counter <= counter - 1'b1;
        end

        monostable_input_old <= monostable_input;
end

endmodule
