`define Note_Std_Half_Duration       19'd416667  // 8.33334 ms


`define Note_Low_DO                  18'd190840  // 3.81680 ms  ->  131 Hz
`define Note_Low_FA                  18'd142857  // 2.85714 ms  ->  175 Hz
`define Note_Low_SOL                 18'd127551  // 2.55102 ms  ->  196 Hz
`define Note_Low_LA                  18'd113636  // 2.27272 ms  ->  220 Hz
`define Note_Low_SI                  18'd101215  // 2.02430 ms  ->  247 Hz

`define Note_DO                      18'd95420   // 1.90840 ms  ->  262 Hz
`define Note_RE                      18'd85034   // 1.70068 ms  ->  294 Hz
`define Note_MI                      18'd75758   // 1.51516 ms  ->  330 Hz
`define Note_FA                      18'd71633   // 1.43266 ms  ->  349 Hz
`define Note_SOL                     18'd63776   // 1.27552 ms  ->  392 Hz
`define Note_LA                      18'd56818   // 1.13636 ms  ->  440 Hz
`define Note_SI                      18'd50607   // 1.01214 ms  ->  494 Hz



module  Module_Audio  #(parameter N = 19)  (input            CLK_50M,
                                            input   [N-1:0]  note_half_duration,
                                            input            mute,
                                            input            stop,
                                            input            reset,
                                            input            point,

                                            output           AUD_L,
                                            output           AUD_R);


reg    [8:0]  counter_melody;
reg   [17:0]  note_half_period_melody;

reg    [8:0]  counter_accomp;
reg   [17:0]  note_half_period_accomp;

reg    [2:0]  counter_point;
reg   [17:0]  note_half_period_point;

reg           rest_melody;
reg           rest_accomp;
reg           rest_point;

reg           play_music_point;

reg           point_old;
reg           metronome_old;

wire          w_metronome;
wire          w_music_melody;
wire          w_music_accomp;
wire          w_music_point;


initial play_music_point = 1'b0;


assign AUD_R = (mute) ? 1'b0 : ((!play_music_point & w_music_melody) |
                                (play_music_point & w_music_point));

assign AUD_L = (mute) ? 1'b0 : (!play_music_point & w_music_accomp);


Module_Frequency_Divider  #(N)   metronome              (.clk_in(CLK_50M),
                                                         .half_period((note_half_duration) ?
                                                                      note_half_duration :
                                                                      `Note_Std_Half_Duration),
                                                         .stop(stop),
                                                         .reset(reset),

                                                         .clk_out(w_metronome));

Module_Frequency_Divider  #(18)  note_generator_melody  (.clk_in(CLK_50M),
                                                         .half_period(note_half_period_melody),
                                                         .stop(stop | rest_melody),
                                                         .reset(reset),

                                                         .clk_out(w_music_melody));

Module_Frequency_Divider  #(18)  note_generator_accomp  (.clk_in(CLK_50M),
                                                         .half_period(note_half_period_accomp),
                                                         .stop(stop | rest_accomp),
                                                         .reset(reset),

                                                         .clk_out(w_music_accomp));

Module_Frequency_Divider  #(18)  note_generator_point   (.clk_in(CLK_50M),
                                                         .half_period(note_half_period_point),
                                                         .stop(stop | !play_music_point |
                                                               rest_point),
                                                         .reset(!point_old & point),

                                                         .clk_out(w_music_point));


always @(posedge CLK_50M) begin
        if (reset) begin
                counter_melody <= 9'd0;
                counter_accomp <= 9'd0;
                counter_point <= 3'd0;
                rest_melody <= 1'b1;
                rest_accomp <= 1'b1;
                rest_point <= 1'b1;
                play_music_point <= 1'b0;
        end else if (!metronome_old & w_metronome) begin
                case (counter_melody)
                        9'd0   : begin
                                rest_melody <= 1'b0;
                                note_half_period_melody <= `Note_SOL;
                        end
                        9'd2   : rest_melody <= 1'b1;
                        9'd6   : rest_melody <= 1'b0;
                        9'd8   : rest_melody <= 1'b1;
                        9'd12  : rest_melody <= 1'b0;
                        9'd14  : rest_melody <= 1'b1;
                        9'd18  : rest_melody <= 1'b0;
                        9'd20  : rest_melody <= 1'b1;
                        9'd24  : rest_melody <= 1'b0;
                        9'd26  : rest_melody <= 1'b1;
                        9'd30  : rest_melody <= 1'b0;
                        9'd32  : rest_melody <= 1'b1;
                        9'd36  : rest_melody <= 1'b0;
                        9'd38  : rest_melody <= 1'b1;
                        9'd42  : rest_melody <= 1'b0;
                        9'd44  : rest_melody <= 1'b1;
                        9'd48  : begin
                                rest_melody <= 1'b0;
                                note_half_period_melody <= `Note_SOL;
                        end
                        9'd60  : note_half_period_melody <= `Note_MI;
                        9'd63  : note_half_period_melody <= `Note_FA;
                        9'd66  : note_half_period_melody <= `Note_SOL;
                        9'd69  : note_half_period_melody <= `Note_LA;
                        9'd72  : note_half_period_melody <= `Note_SOL;
                        9'd78  : note_half_period_melody <= `Note_FA;
                        9'd84  : note_half_period_melody <= `Note_MI;
                        9'd90  : note_half_period_melody <= `Note_RE;
                        9'd96  : note_half_period_melody <= `Note_MI;
                        9'd108 : note_half_period_melody <= `Note_DO;
                        9'd111 : note_half_period_melody <= `Note_RE;
                        9'd114 : note_half_period_melody <= `Note_MI;
                        9'd117 : note_half_period_melody <= `Note_FA;
                        9'd120 : note_half_period_melody <= `Note_MI;
                        9'd126 : note_half_period_melody <= `Note_RE;
                        9'd132 : note_half_period_melody <= `Note_DO;
                        9'd138 : note_half_period_melody <= `Note_Low_SI;
                        9'd144 : note_half_period_melody <= `Note_Low_LA;
                        9'd156 : note_half_period_melody <= `Note_DO;
                        9'd162 : note_half_period_melody <= `Note_LA;
                        9'd165 : note_half_period_melody <= `Note_SOL;
                        9'd180 : note_half_period_melody <= `Note_DO;
                        9'd186 : note_half_period_melody <= `Note_RE;
                        9'd189 : note_half_period_melody <= `Note_MI;
                        9'd192 : note_half_period_melody <= `Note_FA;
                        9'd198 : note_half_period_melody <= `Note_MI;
                        9'd201 : note_half_period_melody <= `Note_RE;
                        9'd207 : note_half_period_melody <= `Note_DO;
                        9'd210 : note_half_period_melody <= `Note_RE;
                        9'd228 : note_half_period_melody <= `Note_DO;
                        9'd234 : note_half_period_melody <= `Note_Low_SI;
                        9'd240 : note_half_period_melody <= `Note_DO;
                        9'd261 : rest_melody <= 1'b1;
                endcase

                if (counter_melody < 9'd263) begin
                        counter_melody <= counter_melody + 9'd1;
                end else begin
                        counter_melody <= 9'd48;
                end

                case (counter_accomp)
                        9'd0   : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_SOL;
                        end
                        9'd2   : rest_accomp <= 1'b1;
                        9'd6   : rest_accomp <= 1'b0;
                        9'd8   : rest_accomp <= 1'b1;
                        9'd12  : rest_accomp <= 1'b0;
                        9'd14  : rest_accomp <= 1'b1;
                        9'd18  : rest_accomp <= 1'b0;
                        9'd20  : rest_accomp <= 1'b1;
                        9'd24  : rest_accomp <= 1'b0;
                        9'd26  : rest_accomp <= 1'b1;
                        9'd30  : rest_accomp <= 1'b0;
                        9'd32  : rest_accomp <= 1'b1;
                        9'd36  : rest_accomp <= 1'b0;
                        9'd38  : rest_accomp <= 1'b1;
                        9'd42  : rest_accomp <= 1'b0;
                        9'd44  : rest_accomp <= 1'b1;
                        9'd48  : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_DO;
                        end
                        9'd51  : rest_accomp <= 1'b1;
                        9'd54  : rest_accomp <= 1'b0;
                        9'd57  : rest_accomp <= 1'b1;
                        9'd60  : rest_accomp <= 1'b0;
                        9'd63  : rest_accomp <= 1'b1;
                        9'd66  : rest_accomp <= 1'b0;
                        9'd69  : rest_accomp <= 1'b1;
                        9'd72  : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_SOL;
                        end
                        9'd75  : rest_accomp <= 1'b1;
                        9'd78  : rest_accomp <= 1'b0;
                        9'd81  : rest_accomp <= 1'b1;
                        9'd84  : rest_accomp <= 1'b0;
                        9'd87  : rest_accomp <= 1'b1;
                        9'd90  : rest_accomp <= 1'b0;
                        9'd93  : rest_accomp <= 1'b1;
                        9'd96  : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_LA;
                        end
                        9'd99  : rest_accomp <= 1'b1;
                        9'd102 : rest_accomp <= 1'b0;
                        9'd105 : rest_accomp <= 1'b1;
                        9'd108 : rest_accomp <= 1'b0;
                        9'd111 : rest_accomp <= 1'b1;
                        9'd114 : rest_accomp <= 1'b0;
                        9'd117 : rest_accomp <= 1'b1;
                        9'd120 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_SOL;
                        end
                        9'd123 : rest_accomp <= 1'b1;
                        9'd126 : rest_accomp <= 1'b0;
                        9'd129 : rest_accomp <= 1'b1;
                        9'd132 : rest_accomp <= 1'b0;
                        9'd135 : rest_accomp <= 1'b1;
                        9'd138 : rest_accomp <= 1'b0;
                        9'd141 : rest_accomp <= 1'b1;
                        9'd144 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_FA;
                        end
                        9'd147 : rest_accomp <= 1'b1;
                        9'd150 : rest_accomp <= 1'b0;
                        9'd153 : rest_accomp <= 1'b1;
                        9'd156 : rest_accomp <= 1'b0;
                        9'd159 : rest_accomp <= 1'b1;
                        9'd162 : rest_accomp <= 1'b0;
                        9'd165 : rest_accomp <= 1'b1;
                        9'd168 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_DO;
                        end
                        9'd171 : rest_accomp <= 1'b1;
                        9'd174 : rest_accomp <= 1'b0;
                        9'd177 : rest_accomp <= 1'b1;
                        9'd180 : rest_accomp <= 1'b0;
                        9'd183 : rest_accomp <= 1'b1;
                        9'd186 : rest_accomp <= 1'b0;
                        9'd189 : rest_accomp <= 1'b1;
                        9'd192 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_LA;
                        end
                        9'd195 : rest_accomp <= 1'b1;
                        9'd198 : rest_accomp <= 1'b0;
                        9'd201 : rest_accomp <= 1'b1;
                        9'd204 : rest_accomp <= 1'b0;
                        9'd207 : rest_accomp <= 1'b1;
                        9'd210 : rest_accomp <= 1'b0;
                        9'd213 : rest_accomp <= 1'b1;
                        9'd216 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_SOL;
                        end
                        9'd219 : rest_accomp <= 1'b1;
                        9'd222 : rest_accomp <= 1'b0;
                        9'd225 : rest_accomp <= 1'b1;
                        9'd228 : rest_accomp <= 1'b0;
                        9'd231 : rest_accomp <= 1'b1;
                        9'd234 : rest_accomp <= 1'b0;
                        9'd237 : rest_accomp <= 1'b1;
                        9'd240 : begin
                                rest_accomp <= 1'b0;
                                note_half_period_accomp <= `Note_Low_FA;
                        end
                        9'd243 : rest_accomp <= 1'b1;
                        9'd246 : rest_accomp <= 1'b0;
                        9'd249 : rest_accomp <= 1'b1;
                        9'd252 : rest_accomp <= 1'b0;
                        9'd255 : rest_accomp <= 1'b1;
                        9'd258 : rest_accomp <= 1'b0;
                        9'd261 : rest_accomp <= 1'b1;
                endcase

                if (counter_accomp < 9'd263) begin
                        counter_accomp <= counter_accomp + 9'd1;
                end else begin
                        counter_accomp <= 9'd48;
                end

                if (play_music_point) begin
                        case (counter_point)
                                3'd0 : begin
                                        rest_point <= 1'b0;
                                        note_half_period_point <= `Note_DO;
                                end
                                3'd1 : note_half_period_point <= `Note_RE;
                                3'd2 : note_half_period_point <= `Note_MI;
                                3'd3 : rest_point <= 1'b1;
                        endcase

                        if (counter_point < 3'd4) begin
                                counter_point <= counter_point + 3'd1;
                        end else begin
                                counter_point <= 3'd0;
                                play_music_point <= 1'b0;
                        end
                end
        end

        if (!point_old & point) begin
                play_music_point <= 1'b1;
        end

        point_old <= point;
        metronome_old <= w_metronome;
end

endmodule
