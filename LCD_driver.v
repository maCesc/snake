`define Waiting_Clk_Cycles_0             20'd750000  // 15000 us
`define Waiting_Clk_Cycles_1             18'd203000  //  4060 us
`define Waiting_Clk_Cycles_2             13'd3000    //    60 us

`define After_Command_Clk_Cycles         11'd2000    //    40 us
`define After_Clear_Clk_Cycles           17'd82000   //  1640 us

`define Function_Set_Command              8'h28
`define Entry_Mode_Set_Command            8'h06
`define Display_On_Off_Command            8'h0C
`define Clear_Display_Command             8'h01
`define Set_DD_RAM_Address_Command        8'h80

`define Prefix_For_Digits                 4'b0011

`define Char_S                            8'b01010011
`define Char_C                            8'b01000011
`define Char_O                            8'b01001111
`define Char_R                            8'b01010010
`define Char_E                            8'b01000101
`define Char_Colon                        8'b00111010
`define Char_Space                        8'b10100000



module  Module_LCD_Driver  (input          CLK_50M,
                            input   [3:0]  units,
                            input   [3:0]  tens,
                            input   [3:0]  hundreds,

                            output  [7:0]  LCD_DB,
                            output         LCD_E,
                            output         LCD_RS,
                            output         LCD_RW);


reg   [19:0]  counter;
reg   [19:0]  limit;
reg    [3:0]  step;

reg    [7:0]  command;
reg           lcd_rs_in;
reg           only_upper_nibble;
reg           send;

reg           initialized;

reg    [3:0]  units_old;
reg    [3:0]  tens_old;
reg    [3:0]  hundreds_old;

wire          w_busy;


initial initialized = 1'b0;


buf (LCD_DB[3:0], 4'b1111);

buf (LCD_RW, 1'b0);


Module_Send_LCD_Command  LCD_controller  (.CLK_50M(CLK_50M),
                                          .command(command),
                                          .lcd_rs_in(lcd_rs_in),
                                          .only_upper_nibble(only_upper_nibble),
                                          .send(send),

                                          .LCD_DB(LCD_DB[7:4]),
                                          .LCD_E(LCD_E),
                                          .LCD_RS(LCD_RS),
                                          .busy(w_busy));


always @(posedge CLK_50M) begin
        if (!initialized) begin
                if (step <= 4'd11) begin
                        if (!counter & !w_busy & !send) begin
                                case (step)
                                        4'd0  : begin
                                                limit <= `Waiting_Clk_Cycles_0;
                                                counter <= 20'd1;
                                        end

                                        4'd2  : begin
                                                limit <= `Waiting_Clk_Cycles_1;
                                                counter <= 20'd1;
                                        end

                                        4'd4  : begin
                                                limit <= `Waiting_Clk_Cycles_2;
                                                counter <= 20'd1;
                                        end

                                        4'd1,
                                        4'd3,
                                        4'd5  : begin
                                                command <= {4'h3, 4'h0};
                                                only_upper_nibble <= 1'b1;
                                                send <= 1'b1;
                                        end

                                        4'd6  : begin
                                                command <= {4'h2, 4'h0};
                                                only_upper_nibble <= 1'b1;
                                                send <= 1'b1;
                                        end

                                        4'd7  : begin
                                                command <= `Function_Set_Command;
                                                only_upper_nibble <= 1'b0;
                                                send <= 1'b1;
                                        end

                                        4'd8  : begin
                                                command <= `Entry_Mode_Set_Command;
                                                only_upper_nibble <= 1'b0;
                                                send <= 1'b1;
                                        end

                                        4'd9  : begin
                                                command <= `Display_On_Off_Command;
                                                only_upper_nibble <= 1'b0;
                                                send <= 1'b1;
                                        end

                                        4'd10 : begin
                                                command <= `Clear_Display_Command;
                                                only_upper_nibble <= 1'b0;
                                                send <= 1'b1;
                                        end

                                        4'd11 : begin
                                                command <= `Set_DD_RAM_Address_Command;
                                                only_upper_nibble <= 1'b0;
                                                send <= 1'b1;
                                        end
                                endcase

                                step <= step + 4'd1;
                        end else if (send) begin
                                send <= 1'b0;
                        end else if (!w_busy) begin
                                if (counter <= limit) begin
                                        counter <= counter + 20'd1;
                                end else begin
                                        counter <= 20'd0;
                                end
                        end
                end else begin
                        step <= 4'd0;
                        only_upper_nibble <= 1'b0;
                        units_old <= 4'b1111;
                        initialized <= 1'b1;
                end
        end else if (units != units_old | tens != tens_old | hundreds != hundreds_old) begin
                if (step <= 4'd10) begin
                        if (!w_busy & !send) begin
                                case (step)
                                        4'd0  : begin
                                                command <= `Clear_Display_Command;
                                                lcd_rs_in <= 1'b0;
                                        end

                                        4'd1  : begin
                                                command <= `Char_S;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd2  : begin
                                                command <= `Char_C;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd3  : begin
                                                command <= `Char_O;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd4  : begin
                                                command <= `Char_R;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd5  : begin
                                                command <= `Char_E;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd6  : begin
                                                command <= `Char_Colon;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd7  : begin
                                                command <= `Char_Space;
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd8  : begin
                                                command <= {`Prefix_For_Digits, hundreds};
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd9  : begin
                                                command <= {`Prefix_For_Digits, tens};
                                                lcd_rs_in <= 1'b1;
                                        end

                                        4'd10 : begin
                                                command <= {`Prefix_For_Digits, units};
                                                lcd_rs_in <= 1'b1;
                                        end
                                endcase

                                send <= 1'b1;
                                step <= step + 4'd1;
                        end else if (send) begin
                                send <= 1'b0;
                        end
                end else begin
                        send <= 1'b0;
                        step <= 4'd0;

                        units_old <= units;
                        tens_old <= tens;
                        hundreds_old <= hundreds;
                end
        end
end

endmodule



module  Module_Send_LCD_Command  (input              CLK_50M,
                                  input       [7:0]  command,
                                  input              lcd_rs_in,
                                  input              only_upper_nibble,
                                  input              send,

                                  output reg  [3:0]  LCD_DB,
                                  output reg         LCD_E,
                                  output reg         LCD_RS,
                                  output reg         busy);


reg  [16:0]  counter;

reg          sending;
reg          lower_nibble;

reg          send_old;


always @(posedge CLK_50M) begin
        if (!busy & !send_old & send) begin
                sending <= 1'b1;
                busy <= 1'b1;
        end else if (sending) begin
                case (counter)
                        17'd0  : begin
                                LCD_RS <= lcd_rs_in;
                                LCD_DB <= (lower_nibble) ? command[3:0] : command[7:4];
                        end

                        17'd2  : LCD_E <= 1'b1;

                        17'd14 : LCD_E <= 1'b0;

                        17'd15 : begin
                                LCD_DB <= 4'd0;
                                LCD_RS <= !lcd_rs_in;
                        end

                        17'd62 : begin
                                counter <= 17'd0;

                                if (!only_upper_nibble) begin
                                        if (lower_nibble) begin
                                                sending <= 1'b0;
                                        end

                                        lower_nibble <= !lower_nibble;
                                end else begin
                                        sending <= 1'b0;
                                end
                        end
                endcase

                if (counter < 17'd62) begin
                        counter <= counter + 17'd1;
                end
        end else if (busy) begin
                if (((command != `Clear_Display_Command | lcd_rs_in) &
                     counter >= `After_Command_Clk_Cycles) |
                    ((command == `Clear_Display_Command & !lcd_rs_in) &
                     counter >= `After_Clear_Clk_Cycles)) begin
                        counter <= 17'd0;
                        busy <= 1'b0;
                end else begin
                        counter <= counter + 17'd1;
                end
        end

        send_old <= send;
end

endmodule
