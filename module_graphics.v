module  Module_Graphics  (input          CLK_50M,
                          input   [1:0]  direction,
                          input   [3:0]  speed,
                          input          go,
                          input          reset,
                          input          show_borders,
                          input          color,

                          output  [3:0]  VGA_B,
                          output  [3:0]  VGA_G,
                          output  [3:0]  VGA_R,
                          output         VGA_HSYNC,
                          output         VGA_VSYNC,
                          output         moving,
                          output         game_over,
                          output         point);


wire         w_clk_VGA;

wire         w_border;
wire         w_prey;
wire         w_snake_head;
wire         w_snake_body;

wire         w_invalid_prey;

wire  [9:0]  wb_x;
wire  [8:0]  wb_y;


Module_VGA_Driver       VGA_driver         (.CLK_50M(CLK_50M),

                                            .VGA_HSYNC(VGA_HSYNC),
                                            .VGA_VSYNC(VGA_VSYNC),
                                            .clk_VGA(w_clk_VGA),
                                            .x(wb_x),
                                            .y(wb_y));

Module_Border           border             (.clk_VGA(w_clk_VGA),
                                            .x(wb_x),
                                            .y(wb_y),
                                            .show_borders(show_borders),

                                            .border(w_border));

Module_Prey             prey               (.clk_VGA(w_clk_VGA),
                                            .x(wb_x),
                                            .y(wb_y),
                                            .generate_prey(reset | point | w_invalid_prey),

                                            .prey(w_prey));

Module_Snake            snake              (.CLK_50M(CLK_50M),
                                            .clk_VGA(w_clk_VGA),
                                            .x(wb_x),
                                            .y(wb_y),
                                            .direction(direction),
                                            .speed(speed),
                                            .go(go),
                                            .game_over(game_over),
                                            .point(point),
                                            .reset(reset),

                                            .snake_head(w_snake_head),
                                            .snake_body(w_snake_body),
                                            .moving(moving));

Module_Check_Collision  collision_checker  (.clk_VGA(w_clk_VGA),
                                            .border(w_border),
                                            .prey(w_prey),
                                            .snake_head(w_snake_head),
                                            .snake_body(w_snake_body),
                                            .reset(reset),

                                            .game_over(game_over),
                                            .point(point),
                                            .invalid_prey(w_invalid_prey));

Module_Paint            painter            (.clk_VGA(w_clk_VGA),
                                            .border(w_border),
                                            .prey(w_prey),
                                            .snake_head(w_snake_head),
                                            .snake_body(w_snake_body),
                                            .color(color),

                                            .VGA_B(VGA_B),
                                            .VGA_G(VGA_G),
                                            .VGA_R(VGA_R));

endmodule
