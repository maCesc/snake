`define Clk_1_kHz_Half_Period       15'd25000  // 0.5 ms
`define Monostable_Duration          3'd5      // 1 * 5 ms


`define Scan_Code_Right_Arrow        8'h74
`define Scan_Code_Left_Arrow         8'h6B
`define Scan_Code_Down_Arrow         8'h72
`define Scan_Code_Up_Arrow           8'h75

`define Key_Up_Code                  8'hF0



module  Module_Keyboard_Driver  (input              PS2_CLK1,
                                 input              PS2_DATA1,

                                 output reg         new_scan_code,
                                 output reg  [7:0]  scan_code);


reg   [3:0]  counter;
reg  [10:0]  data;

reg          parity;


always @(negedge PS2_CLK1) begin
        if (counter >= 4'd1 & counter <= 4'd8 & PS2_DATA1) begin
                parity = !parity;
        end

        data[counter] = PS2_DATA1;
        counter = counter + 4'd1;

        if (counter >= 4'd11) begin
                if (!data[0] & data[10] & data[9] != parity) begin
                        scan_code <= data[8:1];
                        new_scan_code <= 1'b1;
                end

                counter = 4'd0;
                parity = 1'b0;
        end else begin
                new_scan_code <= 1'b0;
        end
end

endmodule



module  Module_Push_Buttons_Controller  (input       CLK_50M,
                                         input       BTN_EAST,
                                         input       BTN_NORTH,
                                         input       BTN_SOUTH,
                                         input       BTN_WEST,

                                         output reg  btn_east_pressed,
                                         output reg  btn_north_pressed,
                                         output reg  btn_south_pressed,
                                         output reg  btn_west_pressed);


reg   btn_east_old;
reg   btn_north_old;
reg   btn_south_old;
reg   btn_west_old;

wire  w_btn_east;
wire  w_btn_north;
wire  w_btn_south;
wire  w_btn_west;

wire  w_clk_monostable;


Module_Frequency_Divider  #(15)  clock_monostable  (.clk_in(CLK_50M),
                                                    .half_period(`Clk_1_kHz_Half_Period),
                                                    .stop(1'b0),
                                                    .reset(1'b0),

                                                    .clk_out(w_clk_monostable));

Module_Monostable_V2      #(3)   monostable_east   (.clk_in(w_clk_monostable),
                                                    .monostable_input(BTN_EAST),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_btn_east));

Module_Monostable_V2      #(3)   monostable_north  (.clk_in(w_clk_monostable),
                                                    .monostable_input(BTN_NORTH),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_btn_north));

Module_Monostable_V2      #(3)   monostable_south  (.clk_in(w_clk_monostable),
                                                    .monostable_input(BTN_SOUTH),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_btn_south));

Module_Monostable_V2      #(3)   monostable_west   (.clk_in(w_clk_monostable),
                                                    .monostable_input(BTN_WEST),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_btn_west));


always @(posedge CLK_50M) begin
        if (!btn_east_old & w_btn_east) begin
                btn_east_pressed <= 1'b1;
        end else begin
                btn_east_pressed <= 1'b0;
        end

        if (!btn_north_old & w_btn_north) begin
                btn_north_pressed <= 1'b1;
        end else begin
                btn_north_pressed <= 1'b0;
        end

        if (!btn_south_old & w_btn_south) begin
                btn_south_pressed <= 1'b1;
        end else begin
                btn_south_pressed <= 1'b0;
        end

        if (!btn_west_old & w_btn_west) begin
                btn_west_pressed <= 1'b1;
        end else begin
                btn_west_pressed <= 1'b0;
        end

        btn_east_old <= w_btn_east;
        btn_north_old <= w_btn_north;
        btn_south_old <= w_btn_south;
        btn_west_old <= w_btn_west;
end

endmodule



module  Module_Control  (input              CLK_50M,
                         input              PS2_CLK1,
                         input              PS2_DATA1,
                         input              BTN_EAST,
                         input              BTN_NORTH,
                         input              BTN_SOUTH,
                         input              BTN_WEST,
                         input              reset,
                         input              stop,

                         output reg  [1:0]  direction,
                         output             go);


reg          go_state;

reg   [3:0]  key_pressed;
reg          keyboard;

reg          new_scan_code_old;
reg   [7:0]  scan_code_old;

wire         w_new_scan_code;
wire  [7:0]  wb_scan_code;

wire         w_btn_east;
wire         w_btn_north;
wire         w_btn_south;
wire         w_btn_west;


assign go = go_state & !stop;


Module_Keyboard_Driver          keyboard_driver          (.PS2_CLK1(PS2_CLK1),
                                                          .PS2_DATA1(PS2_DATA1),

                                                          .new_scan_code(w_new_scan_code),
                                                          .scan_code(wb_scan_code));

Module_Push_Buttons_Controller  push_buttons_controller  (.CLK_50M(CLK_50M),
                                                          .BTN_EAST(BTN_EAST),
                                                          .BTN_NORTH(BTN_NORTH),
                                                          .BTN_SOUTH(BTN_SOUTH),
                                                          .BTN_WEST(BTN_WEST),

                                                          .btn_east_pressed(w_btn_east),
                                                          .btn_north_pressed(w_btn_north),
                                                          .btn_south_pressed(w_btn_south),
                                                          .btn_west_pressed(w_btn_west));


always @(posedge CLK_50M) begin
        if (reset) begin
                go_state <= 1'b0;
                key_pressed <= 4'd0;
                scan_code_old <= 8'd0;
        end else if (!stop) begin
                if (!new_scan_code_old & w_new_scan_code) begin
                        case (wb_scan_code)
                                `Scan_Code_Right_Arrow : begin
                                        if (scan_code_old != `Key_Up_Code | !key_pressed[0]) begin
                                                go_state <= 1'b1;
                                                direction <= 2'b00;  // Right
                                                key_pressed[0] <= 1'b1;
                                                keyboard = 1'b1;
                                        end else begin
                                                key_pressed[0] <= 1'b0;
                                                keyboard = 1'b0;
                                        end
                                end

                                `Scan_Code_Left_Arrow  : begin
                                        if (scan_code_old != `Key_Up_Code | !key_pressed[1]) begin
                                                go_state <= 1'b1;
                                                direction <= 2'b01;  // Left
                                                key_pressed[1] <= 1'b1;
                                                keyboard = 1'b1;
                                        end else begin
                                                key_pressed[1] <= 1'b0;
                                                keyboard = 1'b0;
                                        end
                                end

                                `Scan_Code_Down_Arrow  : begin
                                        if (scan_code_old != `Key_Up_Code | !key_pressed[2]) begin
                                                go_state <= 1'b1;
                                                direction <= 2'b10;  // Down
                                                key_pressed[2] <= 1'b1;
                                                keyboard = 1'b1;
                                        end else begin
                                                key_pressed[2] <= 1'b0;
                                                keyboard = 1'b0;
                                        end
                                end

                                `Scan_Code_Up_Arrow    : begin
                                        if (scan_code_old != `Key_Up_Code | !key_pressed[3]) begin
                                                go_state <= 1'b1;
                                                direction <= 2'b11;  // Up
                                                key_pressed[3] <= 1'b1;
                                                keyboard = 1'b1;
                                        end else begin
                                                key_pressed[3] <= 1'b0;
                                                keyboard = 1'b0;
                                        end
                                end

                                default : keyboard = 1'b0;
                        endcase
                end else begin
                        keyboard = 1'b0;
                end

                if (!keyboard) begin
                        if (w_btn_east) begin
                                go_state <= 1'b1;
                                direction <= 2'b00;  // Right
                        end else if (w_btn_west) begin
                                go_state <= 1'b1;
                                direction <= 2'b01;  // Left
                        end else if (w_btn_south) begin
                                go_state <= 1'b1;
                                direction <= 2'b10;  // Down
                        end else if (w_btn_north) begin
                                go_state <= 1'b1;
                                direction <= 2'b11;  // Up
                        end
                end
        end

        if (!new_scan_code_old & w_new_scan_code) begin
                scan_code_old <= wb_scan_code;
        end

        new_scan_code_old <= w_new_scan_code;
end

endmodule



module  Module_Knob_Controller  #(parameter N = 8)  (input                CLK_50M,
                                                     input                ROT_A,
                                                     input                ROT_B,
                                                     input       [N-1:0]  limit,

                                                     output reg  [N-1:0]  state);


reg   rot_a_old;
reg   rot_b_old;

reg   flag_a;
reg   flag_b;

wire  w_clk_monostable;

wire  w_rot_a;
wire  w_rot_b;


Module_Frequency_Divider  #(15)  clock_monostable  (.clk_in(CLK_50M),
                                                    .half_period(`Clk_1_kHz_Half_Period),
                                                    .stop(1'b0),
                                                    .reset(1'b0),

                                                    .clk_out(w_clk_monostable));

Module_Monostable_V2      #(3)   monostable_a      (.clk_in(w_clk_monostable),
                                                    .monostable_input(ROT_A),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_rot_a));

Module_Monostable_V2      #(3)   monostable_b      (.clk_in(w_clk_monostable),
                                                    .monostable_input(ROT_B),
                                                    .duration(`Monostable_Duration),

                                                    .monostable_output(w_rot_b));


always @(posedge CLK_50M) begin
        if (!rot_a_old & w_rot_a) begin
                if (flag_b) begin
                        flag_b <= 1'b0;

                        if (w_rot_b & state < (limit - 1'b1)) begin
                                state <= state + 1'b1;
			end
                end else if (!w_rot_b) begin
                        flag_a <= 1'b1;
                end
        end else if (!rot_b_old & w_rot_b) begin
		if (flag_a) begin
                        flag_a <= 1'b0;

                        if (w_rot_a & state != 0) begin
                                state <= state - 1'b1;
                        end
		end else if (!w_rot_a) begin
                        flag_b <= 1'b1;
                end
	end

	rot_a_old <= w_rot_a;
        rot_b_old <= w_rot_b;
end

endmodule
