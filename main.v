module  Main  (input          CLK_50M,
               input          BTN_EAST,
               input          BTN_NORTH,
               input          BTN_SOUTH,
               input          BTN_WEST,
               input   [3:0]  SW,
               input          ROT_CENTER,
               input          ROT_A,
               input          ROT_B,
               input          PS2_CLK1,
               input          PS2_DATA1,

               output  [7:0]  LED,
               output  [3:0]  VGA_B,
               output  [3:0]  VGA_G,
               output  [3:0]  VGA_R,
               output         VGA_HSYNC,
               output         VGA_VSYNC,
               output  [7:0]  LCD_DB,
               output         LCD_E,
               output         LCD_RS,
               output         LCD_RW,
               output         AUD_L,
               output         AUD_R);


wire  [1:0]  wb_direction;
wire  [3:0]  wb_speed;

wire  [3:0]  wb_units;
wire  [3:0]  wb_tens;
wire  [3:0]  wb_hundreds;

wire         w_go;
wire         w_moving;
wire         w_game_over;
wire         w_point;


assign LED = (8'd1 << wb_speed) - 8'd1;


Module_Graphics                 graphics             (.CLK_50M(CLK_50M),
                                                      .direction(wb_direction),
                                                      .speed(wb_speed),
                                                      .go(w_go),
                                                      .reset(ROT_CENTER),
                                                      .show_borders(SW[1]),
                                                      .color(SW[2]),

                                                      .VGA_B(VGA_B),
                                                      .VGA_G(VGA_G),
                                                      .VGA_R(VGA_R),
                                                      .VGA_HSYNC(VGA_HSYNC),
                                                      .VGA_VSYNC(VGA_VSYNC),
                                                      .moving(w_moving),
                                                      .game_over(w_game_over),
                                                      .point(w_point));

Module_Control                  movement_controller  (.CLK_50M(CLK_50M),
                                                      .PS2_CLK1(PS2_CLK1),
                                                      .PS2_DATA1(PS2_DATA1),
                                                      .BTN_EAST(BTN_EAST),
                                                      .BTN_NORTH(BTN_NORTH),
                                                      .BTN_SOUTH(BTN_SOUTH),
                                                      .BTN_WEST(BTN_WEST),
                                                      .reset(ROT_CENTER),
                                                      .stop(!SW[0]),

                                                      .direction(wb_direction),
                                                      .go(w_go));

Module_Knob_Controller  #(4)    speed_controller     (.CLK_50M(CLK_50M),
                                                      .ROT_A(ROT_A),
                                                      .ROT_B(ROT_B),
                                                      .limit(`Speed_Limit),

                                                      .state(wb_speed));

Module_3_Digit_Synchro_Counter  point_counter        (.master_clk(CLK_50M),
                                                      .clk_in(w_point),
                                                      .reset(ROT_CENTER),

                                                      .units(wb_units),
                                                      .tens(wb_tens),
                                                      .hundreds(wb_hundreds));

Module_LCD_Driver               LCD_driver           (.CLK_50M(CLK_50M),
                                                      .units(wb_units),
                                                      .tens(wb_tens),
                                                      .hundreds(wb_hundreds),

                                                      .LCD_DB(LCD_DB),
                                                      .LCD_E(LCD_E),
                                                      .LCD_RS(LCD_RS),
                                                      .LCD_RW(LCD_RW));

Module_Audio  #(22)             sound_generator      (.CLK_50M(CLK_50M),
                                                      .note_half_duration(`Note_Std_Half_Duration *
                                                                          (`Speed_Limit -
                                                                           wb_speed)),
                                                      .mute(!SW[3]),
                                                      .stop(!w_go | !w_moving | w_game_over),
                                                      .reset(ROT_CENTER),
                                                      .point(w_point),

                                                      .AUD_L(AUD_L),
                                                      .AUD_R(AUD_R));

endmodule
