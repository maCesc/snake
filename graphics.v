`define Clk_VGA_Half_Period             1'b1
`define Clk_Update_Half_Period         20'd625000    //  25 ms
`define Speed_Limit                     4'd9
`define Clk_Blinking_Half_Period       24'd12500000  // 500 ms


`define HS_Disp                        10'd640
`define HS_PW                          10'd96
`define HS_FP                          10'd16
`define HS_BP                          10'd48

`define HS_Tot                         (`HS_Disp + `HS_PW + `HS_FP + `HS_BP)
`define HS_StartDisp                   (`HS_PW + `HS_BP - 10'd1)
`define HS_StopDisp                    (`HS_StartDisp + `HS_Disp)

`define VS_Disp                        19'd384000
`define VS_PW                          19'd1600
`define VS_FP                          19'd4800   // 19'd8000
`define VS_BP                          19'd26400  // 19'd23200

`define VS_Tot                         (`VS_Disp + `VS_PW + `VS_FP + `VS_BP)
`define VS_StartDisp                   (`VS_PW + `VS_BP)
`define VS_StopDisp                    (`VS_StartDisp + `VS_Disp)


`define Number_Columns                 `HS_Disp
`define Number_Rows                     9'd480

`define Square_Size                     5'd9
`define Square_Size_Plus_One           (`Square_Size + 5'd1)
`define Two_Square_Size_Plus_One       (`Square_Size_Plus_One << 1)

`define Prey_Ini_X                      9'd400
`define Prey_Ini_Y                      7'd110

`define Snake_Ini_X                     7'd100
`define Snake_Ini_Y                     8'd230
`define Snake_Ini_Length                3'd4
`define Snake_Max_Length                7'd100



module  Module_VGA_Driver  (input              CLK_50M,

                            output reg         VGA_HSYNC,
                            output reg         VGA_VSYNC,
                            output             clk_VGA,
                            output reg  [9:0]  x,
                            output reg  [8:0]  y);


reg    [9:0]  HS_count;
reg   [18:0]  VS_count;
reg    [8:0]  rows_count;


Module_Frequency_Divider  #(1)  clock_VGA  (.clk_in(CLK_50M),
                                            .half_period(`Clk_VGA_Half_Period),
                                            .stop(1'b0),
                                            .reset(1'b0),

                                            .clk_out(clk_VGA));


always @(posedge clk_VGA) begin
        if (HS_count >= (`HS_Tot - 10'd1)) begin
                HS_count <= 10'd0;

                if (VS_count >= `VS_StartDisp & VS_count < `VS_StopDisp) begin
                        rows_count <= rows_count + 9'd1;
                end else begin
                        rows_count <= 9'd0;
                end
        end else begin
                HS_count <= HS_count + 10'd1;
        end

        if (VS_count >= (`VS_Tot - 19'd1)) begin
                VS_count <= 19'd0;
        end else begin
                VS_count <= VS_count + 19'd1;
        end

        if (HS_count >= `HS_StartDisp & HS_count < `HS_StopDisp &
            VS_count >= `VS_StartDisp & VS_count < `VS_StopDisp) begin
                x <= HS_count - `HS_StartDisp;
                y <= rows_count;
        end else begin
                x <= 10'd1023;
                y <= 9'd511;
        end

        VGA_HSYNC <= (HS_count < `HS_PW)? 1'b0 : 1'b1;
        VGA_VSYNC <= (VS_count < `VS_PW)? 1'b0 : 1'b1;
end

endmodule



module  Module_Border  (input              clk_VGA,
                        input       [9:0]  x,
                        input       [8:0]  y,
                        input              show_borders,

                        output reg         border);


always @(posedge clk_VGA) begin
        if (show_borders) begin
                if ((x < `Square_Size | x >= (`Number_Columns - `Square_Size) |
                     y < `Square_Size | y >= (`Number_Rows - `Square_Size))
                    & x != 10'd1023 & y != 9'd511) begin
                        border = 1'b1;
                end else begin
                        border = 1'b0;
                end
        end else begin
                border = 1'b0;
        end
end

endmodule



module  Module_Prey  (input              clk_VGA,
                      input       [9:0]  x,
                      input       [8:0]  y,
                      input              generate_prey,

                      output reg         prey);


reg  [9:0]  prey_x;
reg  [8:0]  prey_y;

reg  [9:0]  rand_x;
reg  [8:0]  rand_y;


always @(posedge clk_VGA) begin
        if (!prey_x) begin
                prey = 1'b0;
                prey_x <= `Prey_Ini_X;
                prey_y <= `Prey_Ini_Y;
                rand_x <= `Square_Size_Plus_One;
                rand_y <= `Square_Size_Plus_One;
        end else if (generate_prey) begin
                prey = 1'b0;
                prey_x <= rand_x;
                prey_y <= rand_y;
        end else if (x >= prey_x & x < (prey_x + `Square_Size) &
                     y >= prey_y & y < (prey_y + `Square_Size)) begin
                prey = 1'b1;
        end else begin
                prey = 1'b0;
        end

        if (rand_x >= (`Number_Columns - `Two_Square_Size_Plus_One)) begin
                rand_x <= `Square_Size_Plus_One;
        end else begin
                rand_x <= rand_x + `Square_Size_Plus_One;
        end

        if (rand_y >= (`Number_Rows - `Two_Square_Size_Plus_One)) begin
                rand_y <= `Square_Size_Plus_One;
        end else begin
                rand_y <= rand_y + `Square_Size_Plus_One;
        end
end

endmodule



module  Module_Snake  (input              CLK_50M,
                       input              clk_VGA,
                       input       [9:0]  x,
                       input       [8:0]  y,
                       input       [1:0]  direction,
                       input       [3:0]  speed,
                       input              go,
                       input              game_over,
                       input              point,
                       input              reset,

                       output reg         snake_head,
                       output reg         snake_body,
                       output reg         moving);


reg   [9:0]  snake_x  [`Snake_Max_Length-1:0];
reg   [8:0]  snake_y  [`Snake_Max_Length-1:0];

reg   [6:0]  snake_length;

reg   [1:0]  current_direction;
reg   [1:0]  prohibited_direction;

reg   [6:0]  i;

reg          initialized;

reg          clk_update_old;
reg          point_old;

wire         w_clk_update;
wire         w_clk_blinking;


Module_Frequency_Divider  #(23)  clock_update    (.clk_in(clk_VGA),
                                                  .half_period(`Clk_Update_Half_Period *
                                                               (`Speed_Limit - speed)),
                                                  .stop(1'b0),
                                                  .reset(1'b0),

                                                  .clk_out(w_clk_update));

Module_Frequency_Divider  #(24)  clock_blinking  (.clk_in(clk_VGA),
                                                  .half_period(`Clk_Blinking_Half_Period),
                                                  .stop(1'b0),
                                                  .reset(game_over),

                                                  .clk_out(w_clk_blinking));


always @(posedge CLK_50M) begin
        if (reset | !initialized) begin
                initialized <= 1'b1;
                moving <= 1'b0;

                current_direction <= 2'b01;
                prohibited_direction <= 2'b01;

                snake_length <= `Snake_Ini_Length;

                for (i = 0; i < `Snake_Ini_Length; i = i + 1) begin
                        snake_x[i] <= `Snake_Ini_X - `Square_Size_Plus_One * i;
                        snake_y[i] <= `Snake_Ini_Y;
                end

                for (i = `Snake_Ini_Length; i < `Snake_Max_Length; i = i + 1) begin
                        snake_x[i] <= `Number_Columns;
                        snake_y[i] <= `Number_Rows;
                end
        end else if (!clk_update_old & w_clk_update & go & !game_over &
                     current_direction != prohibited_direction) begin
                moving <= 1'b1;

                for (i = 1; i < `Snake_Max_Length; i = i + 1) begin
                        if (i < snake_length) begin
                                snake_x[i] <= snake_x[i-1];
                                snake_y[i] <= snake_y[i-1];
                        end
                end

                case (current_direction)
                        2'b00 : begin  // Right
                                if (snake_x[0] >= (`Number_Columns - `Square_Size_Plus_One)) begin
                                        snake_x[0] <= 10'd0;
                                end else begin
                                        snake_x[0] <= snake_x[0] + `Square_Size_Plus_One;
                                end

                                prohibited_direction <= 2'b01;
                        end

                        2'b01 : begin  // Left
                                if (snake_x[0] <= 10'd0) begin
                                        snake_x[0] <= `Number_Columns - `Square_Size_Plus_One;
                                end else begin
                                        snake_x[0] <= snake_x[0] - `Square_Size_Plus_One;
                                end

                                prohibited_direction <= 2'b00;
                        end

                        2'b10 : begin  // Down
                                if (snake_y[0] >= (`Number_Rows - `Square_Size_Plus_One)) begin
                                        snake_y[0] <= 9'd0;
                                end else begin
                                        snake_y[0] <= snake_y[0] + `Square_Size_Plus_One;
                                end

                                prohibited_direction <= 2'b11;
                        end

                        2'b11 : begin  // Up
                                if (snake_y[0] <= 9'd0) begin
                                        snake_y[0] <= `Number_Rows - `Square_Size_Plus_One;
                                end else begin
                                        snake_y[0] <= snake_y[0] - `Square_Size_Plus_One;
                                end

                                prohibited_direction <= 2'b10;
                        end
                endcase
        end else if (go & direction != prohibited_direction) begin
                current_direction <= direction;
        end

        if (!point_old & point & snake_length < `Snake_Max_Length) begin
                snake_length <= snake_length + 7'd1;
        end

        clk_update_old <= w_clk_update;
        point_old <= point;
end


always @(posedge clk_VGA) begin
        snake_body = 1'b0;

        if (!game_over | !w_clk_blinking) begin
                for (i = 1; i < `Snake_Max_Length; i = i + 1) begin
                        if (i < snake_length & !snake_body) begin
                                if (x >= snake_x[i] & x < (snake_x[i] + `Square_Size) &
                                    y >= snake_y[i] & y < (snake_y[i] + `Square_Size)) begin
                                        snake_body = 1'b1;
                                end
                        end
                end

                if (x >= snake_x[0] & x < (snake_x[0] + `Square_Size) &
                    y >= snake_y[0] & y < (snake_y[0] + `Square_Size)) begin
                        snake_head = 1'b1;
                end else begin
                        snake_head = 1'b0;
                end
        end else begin
                snake_head = 1'b0;
        end
end

endmodule



module  Module_Check_Collision  (input       clk_VGA,
                                 input       border,
                                 input       prey,
                                 input       snake_head,
                                 input       snake_body,
                                 input       reset,

                                 output reg  game_over,
                                 output reg  point,
                                 output reg  invalid_prey);


always @(posedge clk_VGA) begin
        if (reset) begin
                game_over <= 1'b0;
        end else if (snake_head & (snake_body | border)) begin
                game_over <= 1'b1;
        end else if (snake_head & prey) begin
                point <= 1'b1;
        end else if (snake_body & prey) begin
                invalid_prey <= 1'b1;
        end else begin
                point <= 1'b0;
                invalid_prey <= 1'b0;
        end
end

endmodule



module  Module_Paint  (input              clk_VGA,
                       input              border,
                       input              prey,
                       input              snake_head,
                       input              snake_body,
                       input              color,

                       output reg  [3:0]  VGA_B,
                       output reg  [3:0]  VGA_G,
                       output reg  [3:0]  VGA_R);


always @(posedge clk_VGA) begin
        if (color) begin
                VGA_B <= {4{snake_head}};
                VGA_G <= {4{snake_body | border}};
                VGA_R <= {4{prey}};
        end else begin
                if (snake_head | snake_body | border | prey) begin
                        VGA_B <= 4'b1111;
                        VGA_G <= 4'b1111;
                        VGA_R <= 4'b1111;
                end else begin
                        VGA_B <= 4'b0000;
                        VGA_G <= 4'b0000;
                        VGA_R <= 4'b0000;
                end
        end
end

endmodule
